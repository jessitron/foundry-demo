# Chapter 1

## The Story Begins

It was a dark and stormy night. I was in the middle of a rebase when I
made the mistake of getting up from my keyboard and walking away.
When the merge conflicts cropped up, I didn't even see them.

A phone call distracted me, and then the dog needed let out. Before the
dog returned a mailman arrived, and after yelling at the dog for
barking, I turned my attention to the package.

Hours later, I returned to my computer and started typing code as if
nothing had happened.

Mike's change!